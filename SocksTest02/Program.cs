﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Ditrans;
using Org.Mentalis.Network.ProxySocket;

namespace SocksTest02
{
    class Program
    {
        static readonly StringCollection Proxies = new StringCollection()
        {
            "95.85.59.70:1941", "95.85.59.70:1942", "95.85.59.70:1943", "95.85.59.70:1944", "95.85.59.70:1945", 
            "95.85.59.70:1946", "95.85.59.70:1947", "95.85.59.70:1948", "95.85.59.70:1949", "95.85.59.70:1950"
        };

        static void Main(string[] args)
        {
            string proxy = Proxies[new Random().Next(0, Proxies.Count)];

            var request = SocksHttpWebRequest.Create("http://ifconfig.me/ip");
            request.Proxy = new WebProxy(proxy);
            //request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36";

            string content = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
        }
    }

}
